`use strict`

const express = require(`express`)

const route = express.Router({mergeParam : true})

// GET
route.get(`/`, (req,res) => { res.send(`Test GET Method`) })
route.get(`/:id`, (req,res) => { res.send(`Test GET Method with parameter id : ${req.params.id}`) })
route.get(`/:id/:name`, (req,res) => { res.send(`Test Get Method with parameter id : ${req.params.id} and name : ${req.params.name}`) })
route.get(`/:id([0-9]{5})`, (req,res) => { res.send(`Test Get Method with regex parameter id  : ${req.params.id}`) })

// POST
route.post(`/`, (req,res) => { res.send(`Test POST Method`) })

// PUT
route.put(`/`, (req,res) => { res.send(`Test PUT Method`) })

// DELETE
route.delete(`/`, (req,res) => { res.send(`Test DELETE Method`) })

module.exports = route
