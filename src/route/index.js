/*
  Create By MrGxxD at September 22th 2018
*/

`use strict`

const express = require(`express`)

const example = require(`./example`)

const route = express.Router({mergeParam : true})

route.use(`/example`, example)

module.exports = route
