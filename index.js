/*
  Create By MrGxxD at September 22th 2018
*/

`use strict`

const express = require(`express`)

const route = require(`./src/route`)

const app = express()

const env = process.env.NODE_ENV || `test`

app.use(`/api`, route)

app.all(`*`, (req,res) => { res.send(`Invalid URL`) })

app.listen(3000)
console.log(`Welcome to ${env} environtment`)
